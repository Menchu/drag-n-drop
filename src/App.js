import "./App.scss";
import React from "react";
import DragNDrop from "./components/DragNDrop";

const data = [
  {
    title: "Importantes y Urgentes",
    items: ["Entregar trimestral", "Organizar calendario 2021"],
  },
  {
    title: "Importantes y NO Urgentes",
    items: [
      "Revisión médica",
      "Hacer layaout Drag&drop",
      "Hacer copia disco duro",
    ],
  },
  {
    title: "No Importantes y Urgentes",
    items: ["LLevar el coche a revisión", "Limpieza Mail"],
  },
  {
    title: "NO Importantes y NO Urgentes",
    items: ["Comprar regalo Susana", "Pedir presupuesto sillas"],
  },
];
function App() {
  return (
    <section className="App">
      <p className="App__header">
        {" "}
        Agenda Cuarta Generación - La Agenda de los Emprendedores
      </p>

      <div className="App__main">
        <div className="App__main--introduction">
          <div className="App__main--introduction-text">
            <h3>Tip de Organización</h3>
            <p>
              Si no sabes como priorizar, ésta es una de las maneras más
              eficientes. Clasifica por:
            </p>
            <p>-Importante y Urgente</p>
            <p>-Importante y No Urgente</p>
            <p>-No Importante y Urgente</p>
            <p>-No Importante y No Urgente</p>
          </div>

          <img
            className="App__main--introduction-img"
            src="/assets/header3.png"
            alt="imagen"
          ></img>
        </div>
        <DragNDrop data={data} />
      </div>
    </section>
  );
}

export default App;
