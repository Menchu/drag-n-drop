import React, { useState, useRef } from "react";

export default function DragNDrop({ data }) {
    const [list, setList] = useState(data);
    const dragItem = useRef();
    const dragNode = useRef();
    const [dragging, setDragging] = useState(false);

    const handletDragStart = (ev, params) => {
        console.log("Starting to drag", params);

        dragItem.current = params;
        dragNode.current = ev.target;
        dragNode.current.addEventListener("dragend", handleDragEnd);
        setTimeout(() => {setDragging(true);}, 0);
    };

    const handleDragEnter = (ev, params) => {
        console.log("Entering a drag target", params);
        const currentItem = dragItem.current;
        if (ev.target !== dragNode.current) {
            console.log("Target is NOT the same as dragged item");
            setList((oldList) => {
                let newList = JSON.parse(JSON.stringify(oldList));
                newList[params.grpIndex].items.splice(params.itemIndex,0,
                    newList[currentItem.grpIndex].items.splice(currentItem.itemIndex,1)[0]);
                dragItem.current = params;
                localStorage.setItem("List", JSON.stringify(newList));
                return newList;
            });
        }
    };

    const handleDragEnd = () => {
        console.log("end drag");
        setDragging(false);
        dragNode.current.removeEventListener("dragend", handleDragEnd);
        dragItem.current = null;
        dragNode.current = null;
    };

    const getStyles = (params) => {
        const currentItem = dragItem.current;
        if (
            currentItem.grpIndex === params.grpIndex && currentItem.itemIndex === params.itemIndex
)
            return "current dnd-item";
        else {
            return "dnd-item";
        }
    };

    return (
        <div className="drag-n-drop">
          
            {list.map((grp, grpIndex) => (
                <div
                    key={grp.title}
                    onDragEnter={
                        dragging && !grp.items.length? (ev) => handleDragEnter(ev, { grpIndex,itemIndex: 0,}): null}
                    className="dnd-group"
                >
                    <div className="group-title"> {grp.title} </div>

                    {grp.items.map((item, itemIndex) => (
                        <div
                            draggable
                            onDragStart={(ev) => {
                                handletDragStart(ev, {grpIndex,itemIndex,});
                                }}
                            onDragEnter={dragging? (ev) => {
                                        handleDragEnter(ev, {grpIndex,itemIndex,});}: null}
                            key={item}
                            className={
                                dragging ? getStyles({grpIndex,itemIndex,}): "dnd-item"}
                        >
                            {item}
                        </div>
                    ))}
                </div>
            ))}
        </div>
       
    );
}
